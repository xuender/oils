package main

import (
	"log"
	"reflect"

	"gitee.com/xuender/oils/pb"
)

func list(slice interface{}) {
	value := reflect.ValueOf(slice).Elem()
	elemType := reflect.TypeOf(slice).Elem().Elem().Elem()
	elem := reflect.New(elemType)
	v := reflect.Append(value, elem)

	elem2 := reflect.New(elemType)
	v = reflect.Append(v, elem2)
	value.Set(v)
}

func main() {
	ts := []*pb.Test{}

	list(&ts)

	log.Println(len(ts))
}
