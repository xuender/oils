package main

import (
	"embed"
	"log"

	"gitee.com/xuender/oils/gui"
)

// nolint
//go:embed www/**
var www embed.FS

func main() {
	width := 600
	height := 300

	s := gui.NewGUI()
	defer s.Close()

	s.HTML("/", `<html>
	<body>
	<button onclick="openA()">新建A窗口</button>
	</body>
	</html>`)
	s.FS("www", www)

	s.Bind("openA", func() {
		_ = s.Open("/a.html", "test", width, height)

		log.Println("openA")
	})
	s.Bind("openB", func() {
		_ = s.Open("/b.html", "test", width, height)
		log.Println("openB")
	})

	_ = s.Open("/", "test", width, height)
	s.Run()
}
