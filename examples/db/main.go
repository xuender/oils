package main

import (
	"fmt"

	"gitee.com/xuender/oils/level"
	"gitee.com/xuender/oils/logs"
	"gitee.com/xuender/oils/pb"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/storage"
)

func main() {
	db, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer db.Close()

	log := logs.NewDebug()
	service := level.NewService(db)
	data := &pb.Test{Data: "x1"}

	service.Put([]byte("t_1"), data)
	data.Data = "x2"
	service.Put([]byte("t_2"), data)
	data.Data = "x3"
	service.Put([]byte("t_3"), data)

	array := []*pb.Test{}
	two := 2
	cursor := service.Query([]byte("t_"), two, &array, nil)

	log.Info(fmt.Sprintf("%v", array))

	array = []*pb.Test{}
	_ = service.Query([]byte("t_"), two, &array, cursor)

	log.Info(fmt.Sprintf("%v", array))
}
