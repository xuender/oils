package level_test

import (
	"bytes"
	"sort"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/level"
	"gitee.com/xuender/oils/matrix"
)

func TestKeyASC(t *testing.T) {
	t.Parallel()

	bs1 := []byte("t_")
	key1 := level.KeyASC(bs1)
	key2 := level.KeyASC(bs1)
	key3 := level.KeyASC(bs1)
	bs2 := matrix.Bytes{key1, key2, key3}

	sort.Sort(bs2)

	assert.True(t, bytes.Equal(key1, bs2[0]))
	assert.True(t, bytes.Equal(key2, bs2[1]))
	assert.True(t, bytes.Equal(key3, bs2[2]))
}

func TestKeyDESC(t *testing.T) {
	t.Parallel()

	bs1 := []byte("t_")
	key1 := level.KeyDESC(bs1)
	key2 := level.KeyDESC(bs1)
	key3 := level.KeyDESC(bs1)
	bs2 := matrix.Bytes{key1, key2, key3}

	sort.Sort(bs2)

	assert.True(t, bytes.Equal(key3, bs2[0]))
	assert.True(t, bytes.Equal(key2, bs2[1]))
	assert.True(t, bytes.Equal(key1, bs2[2]))
}
