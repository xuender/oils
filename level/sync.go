package level

import "sync"

func (p *Service) getMutex(key []byte) *sync.Mutex {
	var lock *sync.Mutex

	ks := string(key)
	if m, ok := p.seqMap.Load(ks); ok {
		lock, _ = m.(*sync.Mutex)
	} else {
		m, _ := p.seqMap.LoadOrStore(ks, &sync.Mutex{})
		lock, _ = m.(*sync.Mutex)
	}

	return lock
}
