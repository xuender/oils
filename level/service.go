package level

import "sync"

// Service 数据库服务.
type Service struct {
	DB     DB
	seqMap sync.Map
}

// NewService 新建数据库服务.
func NewService(db DB) *Service {
	return &Service{
		DB:     db,
		seqMap: sync.Map{},
	}
}

// Close 关闭.
func (p *Service) Close() error {
	return p.DB.Close()
}
