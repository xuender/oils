package level_test

import (
	"testing"
	"time"

	"gitee.com/xuender/oils/ids"
	"gitee.com/xuender/oils/level"
	"gitee.com/xuender/oils/pb"
	"github.com/golang/mock/gomock"
)

func TestService_Put(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := NewMockDB(ctrl)
	db.EXPECT().Put([]byte("123"), gomock.Any(), nil).Return(nil)

	td := &pb.Test{ID: 1}
	s := level.NewService(db)
	s.Put([]byte("123"), td)
}

func TestService_PutBytes(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := NewMockDB(ctrl)
	db.EXPECT().Put([]byte("123"), gomock.Any(), nil).Return(nil)
	db.EXPECT().Put([]byte("123"), gomock.Any(), nil).Return(nil)

	id := ids.New()
	s := level.NewService(db)
	s.Put([]byte("123"), &id)
	s.Put([]byte("123"), id)
}

func TestService_PutGob(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := NewMockDB(ctrl)
	db.EXPECT().Put([]byte("123"), gomock.Any(), nil).Return(nil)

	td := time.Now()
	s := level.NewService(db)
	s.Put([]byte("123"), td)
}

func TestService_PutBs(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := NewMockDB(ctrl)
	db.EXPECT().Put([]byte("123"), gomock.Any(), nil).Return(nil)

	td := []byte("xxx")
	s := level.NewService(db)
	s.Put([]byte("123"), td)
}
