package level_test

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/ids"
	"gitee.com/xuender/oils/level"
	"gitee.com/xuender/oils/pb"
	"github.com/golang/mock/gomock"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/storage"
	"google.golang.org/protobuf/proto"
)

func ExampleService_Query() {
	db, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer db.Close()

	service := level.NewService(db)
	test1 := &pb.Test{Data: "x1"}

	service.Put([]byte("t_1"), test1)
	test1.Data = "x2"
	service.Put([]byte("t_2"), test1)
	test1.Data = "x3"
	service.Put([]byte("t_3"), test1)

	tests := []*pb.Test{}

	cursor := service.Query([]byte("t_"), 2, &tests, nil)

	fmt.Println(tests)

	tests = []*pb.Test{}

	_ = service.Query([]byte("t_"), 2, &tests, cursor)
	fmt.Println(tests)

	// Output:
	// [data:"x1" data:"x2"]
	// [data:"x3"]
}

func TestService_QueryDB(t *testing.T) {
	t.Parallel()

	mdb, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer mdb.Close()

	service := level.NewService(mdb)
	service.Put([]byte{1, 2, 1}, "123")
	service.Put([]byte{1, 2, 2}, "123")
	service.Put([]byte{1, 2, 3}, "123")

	list := []string{}
	key := service.Query([]byte{1, 2}, 2, &list, []byte{})

	assert.Equal(t, 2, len(list))
	assert.Equals(t, []byte{1, 2, 3}, key)
}

func TestService_QueryBytes(t *testing.T) {
	t.Parallel()

	db, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer db.Close()

	service := level.NewService(db)
	key1 := []byte{1, 2, 1}
	key2 := []byte{1, 2, 2}
	key3 := []byte{1, 2, 3}
	value1 := []byte{2, 1}
	value2 := []byte{2, 2}
	value3 := []byte{2, 3}

	service.Put(key1, &value1)
	service.Put(key2, &value2)
	service.Put(key3, &value3)

	list := []*[]byte{}
	c := service.Query([]byte{1, 2}, 2, &list, []byte{})

	assert.Equal(t, 2, len(list))
	assert.Equals(t, []byte{1, 2, 3}, c)
	assert.Equals(t, value1, *list[0])
	assert.Equals(t, value2, *list[1])
}

// func TestService_QueryBytes2(t *testing.T) {
// 	t.Parallel()

// 	db, _ := leveldb.Open(storage.NewMemStorage(), nil)
// 	defer db.Close()

// 	s := level.NewService(db)
// 	key1 := []byte{1, 2, 1}
// 	key2 := []byte{1, 2, 2}
// 	key3 := []byte{1, 2, 3}
// 	value1 := []byte{2, 1}
// 	value2 := []byte{2, 2}
// 	value3 := []byte{2, 3}
// 	s.Put(key1, value1)
// 	s.Put(key2, value2)
// 	s.Put(key3, value3)

// 	list := [][]byte{}
// 	c := s.Query([]byte{1, 2}, 2, &list, []byte{})

// 	assert.Equal(t, 2, len(list))
// 	assert.Equal(t, []byte{1, 2, 3}, c)
// 	assert.Equal(t, value1, list[0])
// 	assert.Equal(t, value2, list[1])
// }

func TestService_QueryKey(t *testing.T) {
	t.Parallel()

	memDB, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer memDB.Close()

	service := level.NewService(memDB)
	service.Put([]byte{1, 2, 1}, "123")
	service.Put([]byte{1, 2, 2}, "123")
	service.Put([]byte{1, 2, 3}, "123")

	list := []string{}
	keys := [][]byte{}
	c := service.QueryAndKey([]byte{1, 2}, 2, &list, []byte{}, &keys)

	assert.Equal(t, 2, len(list))
	assert.Equals(t, []byte{1, 2, 3}, c)
	assert.Equals(t, []byte{1, 2, 1}, keys[0])
	assert.Equals(t, []byte{1, 2, 2}, keys[1])
}

func TestService_Query(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	iter := NewMockIterator(ctrl)
	mdb := NewMockDB(ctrl)
	data, _ := proto.Marshal(&pb.Test{Data: "test"})

	mdb.EXPECT().NewIterator(gomock.Any(), gomock.Any()).Return(iter)
	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return(data)

	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return(data)

	iter.EXPECT().Next().Return(false)
	iter.EXPECT().Next().Return(false)

	service := level.NewService(mdb)

	assert.Panics(t, func() {
		_ = service.Query(nil, 0, nil, nil)
	})

	ms := []*pb.Test{}
	c := service.Query([]byte("p_"), 3, &ms, nil)

	assert.Equal(t, 2, len(ms), "数量")
	assert.Equal(t, "test", ms[0].Data)
	assert.Equals(t, []byte{}, c)
}

func TestService_QueryLimit(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	iter := NewMockIterator(ctrl)
	mdb := NewMockDB(ctrl)
	data, _ := proto.Marshal(&pb.Test{Data: "test"})

	mdb.EXPECT().NewIterator(gomock.Any(), gomock.Any()).Return(iter)
	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return(data)
	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Key().Return([]byte("p_2"))

	s := level.NewService(mdb)
	ms := []*pb.Test{}
	c := s.Query([]byte("p_"), 1, &ms, nil)

	assert.Equal(t, 1, len(ms), "数量")
	assert.Equal(t, "test", ms[0].Data)
	assert.Equals(t, []byte("p_2"), c)
}

func TestService_QueryError(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	db := NewMockDB(ctrl)

	s := level.NewService(db)
	num := 1

	assert.Panics(t, func() {
		_ = s.Query([]byte("p_"), 3, &num, nil)
	})
}

func TestService_QueryCorsor(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	iter := NewMockIterator(ctrl)
	mdb := NewMockDB(ctrl)
	data, _ := proto.Marshal(&pb.Test{Data: "test"})

	mdb.EXPECT().NewIterator(gomock.Any(), gomock.Any()).Return(iter)
	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return(data)

	iter.EXPECT().Next().Return(false)
	iter.EXPECT().Next().Return(false)

	s := level.NewService(mdb)
	ms := []*pb.Test{}
	c := s.Query([]byte("p_"), 3, &ms, []byte("p_2"))

	assert.Equal(t, 1, len(ms), "数量")
	assert.Equal(t, "test", ms[0].Data)
	assert.Equals(t, []byte{}, c)
}

func TestService_QueryError2(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	iter := NewMockIterator(ctrl)
	mdb := NewMockDB(ctrl)

	mdb.EXPECT().NewIterator(gomock.Any(), gomock.Any()).Return(iter)
	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return([]byte("123"))

	s := level.NewService(mdb)
	ms := []*pb.Test{}

	assert.Panics(t, func() {
		_ = s.Query([]byte("p_"), 3, &ms, []byte("p_2"))
	})
}

func TestService_QueryID(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	iter := NewMockIterator(ctrl)
	mdb := NewMockDB(ctrl)
	nid := ids.New()

	mdb.EXPECT().NewIterator(gomock.Any(), gomock.Any()).Return(iter)
	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return(nid.Bytes())

	iter.EXPECT().Next().Return(false)
	iter.EXPECT().Next().Return(false)

	service := level.NewService(mdb)

	ids := []ids.ID{}
	_ = service.Query([]byte("p_"), 3, &ids, nil)

	assert.Equal(t, 1, len(ids), "数量")
	assert.Equal(t, nid, ids[0])
}

func TestService_QueryGob(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	iter := NewMockIterator(ctrl)
	mdb := NewMockDB(ctrl)

	buffer := new(bytes.Buffer)
	encoder := gob.NewEncoder(buffer)

	_ = encoder.Encode(33)

	mdb.EXPECT().NewIterator(gomock.Any(), gomock.Any()).Return(iter)
	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return(buffer.Bytes())

	iter.EXPECT().Next().Return(false)
	iter.EXPECT().Next().Return(false)

	s := level.NewService(mdb)

	ms := []int{}
	_ = s.Query([]byte("p_"), 3, &ms, nil)

	assert.Equal(t, 1, len(ms), "数量")
	assert.Equal(t, 33, ms[0])
}

func TestService_QueryGobError(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	iter := NewMockIterator(ctrl)
	mdb := NewMockDB(ctrl)

	buffer := new(bytes.Buffer)
	encoder := gob.NewEncoder(buffer)

	_ = encoder.Encode("sss")

	mdb.EXPECT().NewIterator(gomock.Any(), gomock.Any()).Return(iter)
	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return(buffer.Bytes())

	s := level.NewService(mdb)
	ms := []int8{}

	assert.Panics(t, func() {
		_ = s.Query([]byte("p_"), 3, &ms, nil)
	})
}

func TestService_QueryAndKey(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	iter := NewMockIterator(ctrl)
	mdb := NewMockDB(ctrl)
	data, _ := proto.Marshal(&pb.Test{Data: "test"})

	mdb.EXPECT().NewIterator(gomock.Any(), gomock.Any()).Return(iter)
	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return(data)
	iter.EXPECT().Key().Return([]byte("p_1"))

	iter.EXPECT().Next().Return(true)
	iter.EXPECT().Value().Return(data)
	iter.EXPECT().Key().Return([]byte("p_2"))

	iter.EXPECT().Next().Return(false)
	iter.EXPECT().Next().Return(false)

	servcie := level.NewService(mdb)
	tests := []*pb.Test{}
	keys := [][]byte{}
	key2 := servcie.QueryAndKey([]byte("p_"), 3, &tests, nil, &keys)

	assert.Equal(t, 2, len(tests), "数量")
	assert.Equal(t, 2, len(keys), "数量")
	assert.Equal(t, "test", tests[0].Data)
	assert.Equals(t, []byte{}, key2)
	assert.Equals(t, []byte("p_1"), keys[0])
}
