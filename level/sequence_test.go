package level_test

import (
	"sync"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/level"
	gomock "github.com/golang/mock/gomock"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/storage"
)

func TestNextSequence(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mdb := NewMockDB(ctrl)
	// 不存在
	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{}, leveldb.ErrNotFound)
	mdb.EXPECT().Put(gomock.Any(), []byte{0, 0, 0, 1}, nil).Return(nil)

	service := level.NewService(mdb)
	sid := service.NextSequence([]byte("123"))

	assert.Equal(t, uint32(1), sid)
	// 存在
	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{0, 0, 0, 8}, nil)
	mdb.EXPECT().Put(gomock.Any(), []byte{0, 0, 0, 9}, nil).Return(nil)

	sid = service.NextSequence([]byte("123"))

	assert.Equal(t, uint32(9), sid)
}

func TestNextSequenceSync(t *testing.T) {
	t.Parallel()

	mdb, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer mdb.Close()

	service := level.NewService(mdb)

	var group sync.WaitGroup

	group.Add(200)

	for i := 0; i < 100; i++ {
		go func() {
			_ = service.NextSequence([]byte("123"))

			group.Done()
		}()

		go func() {
			_ = service.NextSequence([]byte("456"))

			group.Done()
		}()
	}

	group.Wait()

	id1 := service.NextSequence([]byte("123"))
	id2 := service.NextSequence([]byte("456"))

	assert.Equal(t, uint32(101), id1)
	assert.Equal(t, id2, id1)
}

func TestNextSequenceASC(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mdb := NewMockDB(ctrl)

	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{0, 0, 0, 8}, nil)
	mdb.EXPECT().Put(gomock.Any(), []byte{0, 0, 0, 9}, nil).Return(leveldb.ErrClosed)

	service := level.NewService(mdb)

	assert.Panics(t, func() {
		_ = service.NextSequenceASC([]byte("456"))
	})

	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{}, leveldb.ErrNotFound)
	mdb.EXPECT().Put(gomock.Any(), []byte{0, 0, 0, 1}, nil).Return(nil)

	id := service.NextSequenceASC([]byte("456"))

	assert.Equals(t, append([]byte("456"), []byte{0, 0, 0, 1}...), id)
}

func TestNextSequenceDESC(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mdb := NewMockDB(ctrl)

	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{}, leveldb.ErrNotFound)
	mdb.EXPECT().Put(gomock.Any(), []byte{0, 0, 0, 1}, nil).Return(nil)

	service := level.NewService(mdb)
	sid := service.NextSequenceDESC([]byte("123"))

	assert.Equals(t, append([]byte("123"), []byte{0xff, 0xff, 0xff, 0xff}...), sid)

	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{0, 0, 0, 8}, nil)
	mdb.EXPECT().Put(gomock.Any(), []byte{0, 0, 0, 9}, nil).Return(leveldb.ErrClosed)

	assert.Panics(t, func() {
		_ = service.NextSequenceDESC([]byte("123"))
	})
}
