package level

// Loader 加载器.
type Loader interface {
	// Load 加载.
	Load([]byte) error
}
