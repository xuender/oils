package level

const (
	// ByteSlice 切片.
	ByteSlice = iota
	// ByteMap map.
	ByteMap
	// ByteSet 集合.
	ByteSet
	// ByteSeq 序列.
	ByteSeq
	// ByteStack 堆栈.
	ByteStack
)

const (
	four  = 4
	eight = 8
)
