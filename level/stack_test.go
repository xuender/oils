package level_test

import (
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/level"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/storage"
)

func TestService_Push(t *testing.T) {
	t.Parallel()

	db, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer db.Close()

	key := []byte("123")
	service := level.NewService(db)

	service.Push(key, "test1")
	service.Push(key)
	service.Push(key, "test2")
	service.Push(key, "test3")

	assert.Equal(t, 3, service.StackSize(key))

	data1 := ""
	data2 := ""
	data3 := ""

	_ = service.Pop(key, &data3, &data2)
	_ = service.Pop(key)
	_ = service.Pop(key, &data1)

	assert.Equal(t, "test3", data3)
	assert.Equal(t, "test2", data2)
	assert.Equal(t, "test1", data1)
}
