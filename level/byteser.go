package level

// Byteser 转换字节码.
type Byteser interface {
	Bytes() []byte
}
