package level

import (
	"encoding/binary"
	"time"
)

// KeyASC 顺序Key.
func KeyASC(prefix []byte) []byte {
	return KeyASCByInt(prefix, time.Now().UnixNano())
}

// KeyASCByInt 根据整数生成顺序Key.
func KeyASCByInt(prefix []byte, id int64) []byte {
	bs := make([]byte, eight)

	binary.BigEndian.PutUint64(bs, uint64(id))

	return append(prefix, bs...)
}

// KeyASCByUint32 根据整数生成顺序Key.
func KeyASCByUint32(prefix []byte, id uint32) []byte {
	bs := make([]byte, four)

	binary.BigEndian.PutUint32(bs, id)

	return append(prefix, bs...)
}

// KeyDESC 倒序Key.
func KeyDESC(prefix []byte) []byte {
	return KeyDESCByInt(prefix, time.Now().UnixNano())
}

// KeyDESCByInt 根据整数生成倒序Key.
func KeyDESCByInt(prefix []byte, id int64) []byte {
	bs := make([]byte, eight)

	binary.BigEndian.PutUint64(bs, uint64(0-id))

	return append(prefix, bs...)
}

// KeyDESCByUint32 uint32 倒序.
func KeyDESCByUint32(prefix []byte, id uint32) []byte {
	bs := make([]byte, four)

	binary.BigEndian.PutUint32(bs, 0-id)

	return append(prefix, bs...)
}
