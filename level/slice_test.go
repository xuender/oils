package level_test

import (
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/level"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/storage"
)

func TestService_Append(t *testing.T) {
	t.Parallel()

	mdb, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer mdb.Close()

	key := []byte("123")
	service := level.NewService(mdb)

	service.Append(key, "1", "2", "3")

	array := []string{}

	service.Slice(key, &array)

	assert.Equal(t, 3, len(array))
	assert.Equal(t, "1", array[0])
	assert.Equal(t, "2", array[1])
	assert.Equal(t, "3", array[2])
}
