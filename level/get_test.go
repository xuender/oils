package level_test

import (
	"bytes"
	"encoding/gob"
	"errors"
	"sort"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/base"
	"gitee.com/xuender/oils/ids"
	"gitee.com/xuender/oils/level"
	"gitee.com/xuender/oils/pb"
	"gitee.com/xuender/oils/tags"
	"github.com/golang/mock/gomock"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/storage"
	"google.golang.org/protobuf/proto"
)

var Err = errors.New("error")

func TestService_Get(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mdb := NewMockDB(ctrl)
	data, _ := proto.Marshal(&pb.Test{ID: 3})
	mdb.EXPECT().Get(gomock.Any(), nil).Return(data, nil)
	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{}, Err)
	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{}, Err)

	testData := new(pb.Test)
	service := level.NewService(mdb)

	assert.True(t, service.Get([]byte("123"), testData))
	assert.Equal(t, uint32(3), testData.ID)

	assert.Panics(t, func() {
		service.Get([]byte("456"), testData)
	})

	assert.Panics(t, func() {
		service.Get([]byte("456"), 3)
	})
}

func TestService_GetError(t *testing.T) {
	t.Parallel()

	db, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer db.Close()

	td := new(pb.Test)
	s := level.NewService(db)

	assert.False(t, s.Get([]byte("123"), td))
}

func TestService_GetLoader(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mdb := NewMockDB(ctrl)
	testID := ids.New()
	mdb.EXPECT().Get(gomock.Any(), nil).Return(testID.Bytes(), nil)
	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{}, Err)

	testData := ids.New()
	service := level.NewService(mdb)
	service.Get([]byte("123"), &testData)
	assert.Equal(t, testID, testData)

	assert.Panics(t, func() {
		service.Get([]byte("456"), &testData)
	})
}

func TestService_GetGod(t *testing.T) {
	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mdb := NewMockDB(ctrl)
	testID := int64(3)

	buffer := new(bytes.Buffer)
	encoder := gob.NewEncoder(buffer)
	_ = encoder.Encode(testID)

	mdb.EXPECT().Get(gomock.Any(), nil).Return(buffer.Bytes(), nil)
	mdb.EXPECT().Get(gomock.Any(), nil).Return([]byte{}, Err)

	newID := int64(0)
	s := level.NewService(mdb)
	s.Get([]byte("123"), &newID)
	assert.Equal(t, testID, newID)

	assert.Panics(t, func() {
		s.Get([]byte("456"), &newID)
	})
}

func TestService_GetBytes(t *testing.T) {
	t.Parallel()

	db, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer db.Close()

	service := level.NewService(db)

	bytes := []byte("123")
	service.Put(bytes, bytes)

	ret := []byte{}
	service.Get(bytes, &ret)

	assert.Equals(t, ret, bytes)
}

func TestService_GetTags(t *testing.T) {
	t.Parallel()

	db, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer db.Close()

	servie := level.NewService(db)

	bs := []byte("123")
	tag := tags.NewTags(1, 2)
	servie.Put(bs, &tag)

	ns := tags.NewTags[int]()
	servie.Get(bs, &ns)

	assert.Equals(t, ns, tag)
}

func TestService_GetStrings(t *testing.T) {
	t.Parallel()

	db, _ := leveldb.Open(storage.NewMemStorage(), nil)
	defer db.Close()

	service := level.NewService(db)

	bs := []byte("123")
	set := base.NewSet("1", "2", "3")
	service.Put(bs, set)

	set2 := base.NewSet[string]()
	service.Get(bs, &set2)

	slice1 := set.Slice()
	sort.Strings(slice1)

	slice2 := set2.Slice()
	sort.Strings(slice2)
	assert.Equals(t, slice1, slice2)
}

func TestBytes2Interface(t *testing.T) {
	t.Parallel()

	s := "123"
	bs := level.Bytes(s)
	n := ""

	_ = level.Bytes2Interface(bs, &n)
	assert.Equal(t, s, n)
}
