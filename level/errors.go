package level

import "errors"

// ErrNotPtr 参数不是指针.
var ErrNotPtr = errors.New("参数不是指针")

// ErrBedType 错误的类型.
var ErrBedType = errors.New("未知类型")

// ErrNotSlice 参数不是切片.
var ErrNotSlice = errors.New("参数不是切片")
