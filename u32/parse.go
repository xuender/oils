package u32

import (
	"time"

	"gitee.com/xuender/oils/base"
)

func Parse(str string) uint32 {
	ret, _ := base.ParseInteger[uint32](str)

	return ret
}

func ParseTime(input time.Time) uint32 {
	return uint32(input.Year()*10000 + int(input.Month())*100 + input.Day())
}
