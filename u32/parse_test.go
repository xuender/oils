package u32_test

import (
	"testing"
	"time"

	. "gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/base"
	"gitee.com/xuender/oils/u32"
)

func TestParse(t *testing.T) {
	t.Parallel()

	Equal(t, 1, u32.Parse("1"))
}

func TestParseTime(t *testing.T) {
	t.Parallel()

	day := base.Panic1(time.Parse("20060102", "20220424"))
	Equal(t, 20220424, u32.ParseTime(day))
}
