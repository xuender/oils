package u32_test

import (
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/u32"
)

func TestU32_Bytes(t *testing.T) {
	t.Parallel()

	u := u32.U32(33)
	bs := u.Bytes()

	assert.Equals(t, []byte{0, 0, 0, 33}, bs)
}

func TestUint32(t *testing.T) {
	t.Parallel()

	i := u32.Uint32([]byte{0, 0, 0, 33})

	assert.Equal(t, uint32(33), i)
}
