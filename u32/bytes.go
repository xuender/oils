package u32

import "encoding/binary"

// Bytes uint32 to []byte.
func Bytes(i uint32) []byte {
	four := 4
	bytes := make([]byte, four)

	binary.BigEndian.PutUint32(bytes, i)

	return bytes
}

// Uint32 []byte to Uint32.
func Uint32(bytes []byte) uint32 {
	return binary.BigEndian.Uint32(bytes)
}

// Bytes 转换字节码.
func (u U32) Bytes() []byte {
	return Bytes(uint32(u))
}
