package matrix_test

import (
	"fmt"
	"sort"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/matrix"
)

func ExampleBytes() {
	bytes := matrix.Bytes{[]byte("world"), []byte("hello")}

	fmt.Println(string(bytes[0]))
	fmt.Println(string(bytes[1]))
	fmt.Println(bytes.All([]byte("hello"), []byte("world")))
	fmt.Println(bytes.Any([]byte("xxx"), []byte("hello")))
	fmt.Println(bytes.Has([]byte("hello")))
	fmt.Println(bytes.Index([]byte("world")))
	fmt.Println(bytes.Index([]byte("xxx")))
	sort.Sort(bytes)
	fmt.Println(bytes)

	// Output:
	// world
	// hello
	// true
	// true
	// true
	// 0
	// -1
	// hello, world
}

func TestBytes_Join(t *testing.T) {
	t.Parallel()

	bytes := matrix.Bytes{[]byte("world"), []byte("hello")}

	sort.Sort(bytes)

	assert.Equal(t, "hello-world", bytes.Join("-"))

	bytes.Del(0)
	assert.Equal(t, "world", bytes.Join("-"))

	bytes.Del(0)
	assert.Equal(t, "", bytes.Join("-"))
}

func TestBytes_Del(t *testing.T) {
	t.Parallel()

	bytes := matrix.Bytes{[]byte("world"), []byte("hello")}

	bytes.Del(100)
	assert.Equal(t, 2, len(bytes))
}

func TestBytes_Any(t *testing.T) {
	t.Parallel()

	bytes := matrix.Bytes{[]byte("world"), []byte("hello")}

	assert.True(t, bytes.Any([]byte("china"), []byte("hello")))
	assert.False(t, bytes.Any([]byte("china"), []byte("good")))
}

func TestBytes_All(t *testing.T) {
	t.Parallel()

	bytes := matrix.Bytes{[]byte("world"), []byte("hello")}

	assert.True(t, bytes.All([]byte("world"), []byte("hello")))
	assert.False(t, bytes.All([]byte("world"), []byte("good")))
}

func TestBytes_Equal(t *testing.T) {
	t.Parallel()

	byte1 := matrix.Bytes{[]byte("world"), []byte("hello")}
	byte2 := matrix.Bytes{[]byte("world"), []byte("hello")}
	byte3 := matrix.Bytes{[]byte("china"), []byte("hello")}
	byte4 := matrix.Bytes{[]byte("hello")}

	assert.True(t, byte1.Equal(byte2))
	assert.False(t, byte1.Equal(byte3))
	assert.False(t, byte1.Equal(byte4))
}
