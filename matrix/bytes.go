package matrix

import (
	"bytes"
	"strings"
)

// Bytes 字节码矩阵.
type Bytes [][]byte

func (x Bytes) Len() int           { return len(x) }
func (x Bytes) Less(i, j int) bool { return bytes.Compare(x[i], x[j]) < 0 }
func (x Bytes) Swap(i, j int)      { x[i], x[j] = x[j], x[i] }

// All 全部包含.
func (x Bytes) All(elems ...[]byte) bool {
	for _, e := range elems {
		if !x.Has(e) {
			return false
		}
	}

	return true
}

// Any 任意包含.
func (x Bytes) Any(elems ...[]byte) bool {
	for _, e := range elems {
		if x.Has(e) {
			return true
		}
	}

	return false
}

// Del 根据序号删除.
func (x *Bytes) Del(index int) Bytes {
	if index >= len(*x) {
		return *x
	}

	*x = append((*x)[:index], (*x)[index+1:]...)

	return *x
}

// Equal 相等.
func (x Bytes) Equal(elem Bytes) bool {
	if len(x) != len(elem) {
		return false
	}

	for i, b := range x {
		if !bytes.Equal(b, elem[i]) {
			return false
		}
	}

	return true
}

// Has 包含.
func (x Bytes) Has(r []byte) bool {
	for _, e := range x {
		if bytes.Equal(e, r) {
			return true
		}
	}

	return false
}

// Index 位置.
func (x Bytes) Index(r []byte) int {
	for i, e := range x {
		if bytes.Equal(e, r) {
			return i
		}
	}

	return -1
}

// Join 集合连接.
func (x Bytes) Join(sep string) string {
	switch len(x) {
	case 0:
		return ""
	case 1:
		return string(x[0])
	}

	var builder strings.Builder

	builder.WriteString(string(x[0]))

	for _, i := range x[1:] {
		builder.WriteString(sep)
		builder.WriteString(string(i))
	}

	return builder.String()
}

// String 转换成字符串.
func (x Bytes) String() string {
	return x.Join(", ")
}
