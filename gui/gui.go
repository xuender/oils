package gui

import (
	"io/fs"
	"net/http"
)

// GUI 图形用户界面.
type GUI interface {
	// Bind 事件绑定.
	Bind(name string, f interface{})
	// HTML html界面.
	HTML(url, html string)
	// FS 文件系统.
	FS(dir string, fsys fs.FS)
	// Handler .
	Handler(http.Handler)
	// Open 打开窗口.
	Open(url, name string, width, height int) error
	// Run 运行.
	Run()
	// Close 关闭.
	Close() error
}
