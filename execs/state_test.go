package execs_test

import (
	"os"
	"path/filepath"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/execs"
)

func TestStateFile(t *testing.T) {
	t.Parallel()

	assert.Equal(t, execs.Play, execs.StateByFile("/tmpfs/play"))
	assert.Equal(t, execs.Build, execs.StateByFile(filepath.Join("aa", "bb")))
	assert.Equal(t, execs.Run, execs.StateByFile(filepath.Join(os.TempDir(), "go-build123", "exe", "main")))
	assert.Equal(t, execs.Test, execs.StateByFile(filepath.Join(os.TempDir(), "go-build123", "main.test")))
}
