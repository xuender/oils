package execs_test

import (
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/execs"
)

func TestExec(t *testing.T) {
	t.Parallel()

	assert.Nil(t, execs.Exec("ls"))
}

func TestExecOut(t *testing.T) {
	t.Parallel()

	o, err := execs.ExecOut("ls")

	assert.Nil(t, err)
	assert.NotNil(t, o)
}
