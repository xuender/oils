# oils

golang 类库

## assert

test assert

## base

* Map
* JSNumber
* Panic
* Set
* Slice

## cache

* Cache

## execs

* Exec
* ExecOut
* Open
* Show

## gui

* GUI
* NewGUI

## hashs

* SipHash128
* SipHash64

## ids

* ID
* IDS

## level

* DB
* Service
* KeyASC
* KeyASCByInt
* KeyDESC
* KeyDESCByInt

## logs

* NewGormLogger
* RotateLog
* NewDebug

## matrix

* Bytes

## nets

* GetIP

## oss

* SignalClose

## syncs

* RoutineGroup

## tags

* Int2Bytes
* Bytes2Ints
* Tags

## times

* ClockStart

## u32

* Bytes
* Uint32
* U32

## u64

## unit

* ByteUnit
