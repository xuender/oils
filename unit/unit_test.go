package unit_test

import (
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/unit"
)

func TestByteUnit(t *testing.T) {
	t.Parallel()
	assert.Equal(t, "3B", unit.ByteUnit(3))
	assert.Equal(t, "3K", unit.ByteUnit(3*1024))
	assert.Equal(t, "3M", unit.ByteUnit(3*1024*1024))

	assert.Equal(t, "3.5K", unit.ByteUnit(1024*3+1024*0.5))
	assert.Equal(t, "3.51K", unit.ByteUnit(1024*3+1024*0.5+10))
}

func TestRound(t *testing.T) {
	t.Parallel()

	assert.Equal(t, "1.51", unit.Round(1.509, 2))
	assert.Equal(t, "1.5", unit.Round(1.504, 2))
}
