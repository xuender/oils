package ids_test

import (
	"fmt"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/ids"
	"github.com/google/uuid"
)

func ExampleShort() {
	fmt.Println(ids.Short("mxSR7WeTuKA8tmEyQhoZU"))

	// Output:
	// mxSR7WeTuKA8tmEyQhoZU <nil>
}

func TestNew(t *testing.T) {
	t.Parallel()

	assert.Equal(t, 16, len(ids.New()))
}

func TestID_Short(t *testing.T) {
	t.Parallel()

	id := ids.New()

	assert.Less(t, 20, len(id.String()))
}

func TestID_String(t *testing.T) {
	t.Parallel()

	id := ids.New()

	assert.Equal(t, 36, len(id.UUID()))
}

func TestShort(t *testing.T) {
	t.Parallel()

	tid := ids.New()
	str := tid.String()
	nid, err := ids.Short(str)

	assert.Nil(t, err)

	for i := 0; i < 16; i++ {
		assert.Equal(t, tid[i], nid[i])
	}

	_, err = ids.Short("11")

	assert.NotNil(t, err)
}

func TestBytes(t *testing.T) {
	t.Parallel()

	id := ids.New()
	ni := ids.Bytes(id[:])

	for i := 0; i < 16; i++ {
		assert.Equal(t, id[i], ni[i])
	}
}

func TestHashID(t *testing.T) {
	t.Parallel()

	h := ids.Hash([]byte("123"))

	assert.Equal(t, "mxSR7WeTuKA8tmEyQhoZUP", h.String())
}

func TestID_Prefix(t *testing.T) {
	t.Parallel()

	data := []byte("123")
	tid := ids.New()
	pid := tid.Prefix(data)

	for i, b := range data {
		assert.Equal(t, b, pid[i])
	}

	for i, b := range tid {
		assert.Equal(t, b, pid[i+len(data)])
	}
}

func TestMachine(t *testing.T) {
	t.Parallel()

	assert.Equal(t, 16, len(ids.Machine("test")))
}

func TestUUID(t *testing.T) {
	t.Parallel()

	_, err := ids.UUID(uuid.New().String())

	assert.Nil(t, err)
}

func TestID_Bytes(t *testing.T) {
	t.Parallel()

	id := ids.New()

	assert.Equal(t, 16, len(id.Bytes()))
}

func TestID_Load(t *testing.T) {
	t.Parallel()

	tid := ids.New()
	nid := ids.New()
	_ = nid.Load(tid[:])

	for i := 0; i < 16; i++ {
		assert.Equal(t, tid[i], nid[i])
	}

	err := nid.Load(tid[3:])
	assert.NotNil(t, err)
}
