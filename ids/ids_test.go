package ids_test

import (
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/ids"
)

func TestIDS_Has(t *testing.T) {
	t.Parallel()

	id := ids.New()
	is := ids.NewIDS(ids.New(), id)

	assert.True(t, is.Has(id))
	assert.False(t, is.Has(ids.New()))
}

func TestIDS_Add(t *testing.T) {
	t.Parallel()

	id := ids.New()
	is := ids.NewIDS(ids.New(), id)
	assert.Equal(t, 2, len(is))

	assert.Equal(t, 3, len(is.Add(ids.New())))
	assert.Equal(t, 3, len(is))
}

func TestIDS_Del(t *testing.T) {
	t.Parallel()

	id := ids.New()
	is := ids.NewIDS(ids.New(), id)

	assert.Equal(t, 2, len(is))
	assert.Equal(t, 1, len(is.Del(id)))
	assert.Equal(t, 1, len(is))
}

func TestIDS_AddBytes(t *testing.T) {
	t.Parallel()

	tis := ids.IDS{}
	id1 := ids.New()
	id2 := ids.New()

	tis.AddBytes([][]byte{id1[:], id2[:]}...)
	assert.Equal(t, 2, len(tis))
	assert.True(t, tis.Has(id1))
	assert.True(t, tis.Has(id2))
}

func TestIDS_Bytes(t *testing.T) {
	t.Parallel()

	is := ids.NewIDS(ids.New(), ids.New())
	data := is.Bytes()

	assert.Equal(t, 2, len(data))
	assert.Equal(t, 16, len(data[0]))
}

func TestIntersection(t *testing.T) {
	t.Parallel()

	tid := ids.New()
	is1 := ids.NewIDS(tid, ids.New(), ids.New())
	is2 := ids.NewIDS(tid, ids.New(), ids.New())

	is3 := ids.Intersection(is1, is2)
	is4 := ids.Intersection(is1)
	is5 := ids.Intersection()

	assert.True(t, is3.Has(tid))
	assert.Equal(t, 1, len(is3))
	assert.Equal(t, 3, len(is4))
	assert.Equal(t, 0, len(is5))
}

func TestUnion(t *testing.T) {
	t.Parallel()

	id1 := ids.New()
	id2 := ids.New()
	is1 := ids.NewIDS(id1, ids.New(), ids.New())
	is2 := ids.NewIDS(id2, ids.New(), ids.New())
	is3 := ids.Union(is1, is2)

	assert.True(t, is3.Has(id1))
	assert.True(t, is3.Has(id2))
}

func TestIDS_All(t *testing.T) {
	t.Parallel()

	id1 := ids.New()
	id2 := ids.New()
	is := ids.NewIDS(id1, ids.New(), ids.New())

	assert.True(t, is.All(id1))
	assert.False(t, is.All(id1, id2))
}

func TestIDS_Any(t *testing.T) {
	t.Parallel()

	id1 := ids.New()
	id2 := ids.New()
	is := ids.NewIDS(id1, ids.New(), ids.New())

	assert.True(t, is.Any(id1))
	assert.True(t, is.Any(id1, id2))
	assert.False(t, is.Any(ids.New()))
}

func TestIDS_AllIDS(t *testing.T) {
	t.Parallel()

	id1 := ids.New()
	id2 := ids.New()
	is := ids.NewIDS(id1, ids.New(), ids.New())

	assert.True(t, is.AllIDS(ids.NewIDS(id1)))
	assert.False(t, is.AllIDS(ids.NewIDS(id1, id2)))
}

func TestIDS_AnyIDS(t *testing.T) {
	t.Parallel()

	id1 := ids.New()
	id2 := ids.New()
	is := ids.NewIDS(id1, ids.New(), ids.New())

	assert.True(t, is.AnyIDS(ids.NewIDS(id1)))
	assert.True(t, is.AnyIDS(ids.NewIDS(id1, id2)))
	assert.False(t, is.AnyIDS(ids.NewIDS(ids.New())))
}

func TestIDS_Complement(t *testing.T) {
	t.Parallel()

	id1 := ids.New()
	id2 := ids.New()
	is1 := ids.NewIDS(id1, ids.New(), ids.New())
	is2 := is1.Complement(ids.NewIDS(id1, id2))

	assert.True(t, is2.Has(id2))
	assert.Equal(t, 1, len(is2))
}

func TestIDS_Slice(t *testing.T) {
	t.Parallel()

	id1 := ids.New()
	is := ids.NewIDS(id1, ids.New(), ids.New())

	is.Add(id1)
	assert.Equal(t, 3, len(is.Slice()))
}

func TestIDS_Marshal(t *testing.T) {
	t.Parallel()

	tid := ids.New()
	nid := ids.NewIDS(tid, ids.New(), ids.New())

	data := nid.Marshal()
	newIS, _ := ids.Unmarshal(data)

	assert.True(t, newIS.Has(tid))
}
