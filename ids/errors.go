package ids

import "errors"

// ErrBytesSize 长度不够.
var ErrBytesSize = errors.New("字节码长度不够")
