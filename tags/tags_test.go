package tags_test

import (
	"fmt"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/tags"
)

func ExampleTags() {
	tag := tags.NewTags(1, 2, 33)

	fmt.Println(tag.Has(1))
	fmt.Println(tag.Has(3))
	fmt.Println(tag.Has(33))
	fmt.Println(tag)

	// Output:
	// true
	// false
	// true
	// 1, 2, 33
}

func TestTags_Has(t *testing.T) {
	t.Parallel()

	myTags := tags.NewTags[uint32]()

	for i := 0; i < 100000; i++ {
		tag := uint32(i)

		assert.False(t, myTags.Has(tag))
		myTags.Add(tag)
		assert.True(t, myTags.Has(tag))
	}

	for i := 0; i < 100000; i++ {
		tag := uint32(i)

		assert.True(t, myTags.Has(tag))
		myTags.Del(tag)
		assert.False(t, myTags.Has(tag))
	}
}

func TestTags_Del(t *testing.T) {
	t.Parallel()

	tag := tags.NewTags(1, 2, 3, 4)
	tag.Del(2, 3)

	assert.True(t, tag.Has(1))
	assert.False(t, tag.Has(2))
	assert.False(t, tag.Has(3))
	assert.True(t, tag.Has(4))
}

func TestTags_All(t *testing.T) {
	t.Parallel()

	ts := tags.NewTags(1, 2, 3, 4)

	assert.True(t, ts.All(1, 3))
	assert.False(t, ts.All(2, 6))
}

func TestTags_Any(t *testing.T) {
	t.Parallel()

	ts := tags.NewTags(1, 2, 3, 4)

	assert.True(t, ts.Any(2, 6))
	assert.False(t, ts.Any(7, 30))
}

func TestTags_Slice(t *testing.T) {
	t.Parallel()

	ts := tags.NewTags(2, 1, 4)

	ts.Add(3)
	assert.Equals(t, []int{1, 2, 3, 4}, ts.Slice())
}

func TestTags_String(t *testing.T) {
	t.Parallel()

	tag := tags.NewTags(2, 1, 4, 33)

	assert.Equal(t, "1, 2, 4, 33", tag.String())

	tag.Del(2)
	tag.Del(1)
	tag.Del(33)
	assert.Equal(t, "4", tag.String())

	tag.Del(4)
	assert.Equal(t, "", tag.String())
}

func TestTags_Add(t *testing.T) {
	t.Parallel()

	tag := tags.NewTags(2, 1, 4)

	assert.Equal(t, 1, len(tag))

	tag.Add(10)
	assert.Equal(t, 2, len(tag))
	assert.Equal(t, 2, len(tag.Add(3)))
	assert.Equal(t, 2, len(tag.Add(7, 11)))
	assert.Equal(t, 13, len(tag.Add(100)))
}

func TestTags_DelTags(t *testing.T) {
	t.Parallel()

	ts1 := tags.NewTags(2, 1, 4)
	ts2 := tags.NewTags(2, 1, 3, 99)

	ts1.DelTags(ts2)

	assert.Equal(t, 1, len(ts1.Slice()))
	assert.Equals(t, []int{4}, ts1.Slice())
	assert.True(t, ts1.Has(4))

	ts1 = tags.NewTags(2, 1, 3, 99)
	ts2 = tags.NewTags(2, 1, 4)

	ts1.DelTags(ts2)

	assert.Equal(t, 2, len(ts1.Slice()))
	assert.Equals(t, []int{3, 99}, ts1.Slice())
	assert.True(t, ts1.Has(3))
}

func TestTags_AddTags(t *testing.T) {
	t.Parallel()

	ts1 := tags.NewTags(2, 1, 4)
	ts2 := tags.NewTags(2, 1, 3, 99)

	ts1.AddTags(ts2)

	assert.Equal(t, 5, len(ts1.Slice()))
}

func TestTags_Count(t *testing.T) {
	t.Parallel()

	tag := tags.NewTags(2, 1, 4, 2, 4)

	assert.Equal(t, 3, tag.Count())
	assert.Equal(t, 3, len(tag.Slice()))
}

func TestIntersection(t *testing.T) {
	t.Parallel()

	ts1 := tags.NewTags(2, 1, 4, 7, 2, 4, 200)
	ts2 := tags.NewTags(2, 1, 3, 4, 2, 18, 4)
	ts3 := tags.NewTags(2, 1, 4, 3)
	tag := tags.Intersection(ts1, ts2, ts3)

	assert.Equal(t, 3, tag.Count())
	assert.True(t, tag.Has(2))
	assert.True(t, tag.Has(1))
	assert.True(t, tag.Has(4))

	tag = tags.Intersection(ts1)
	assert.Equals(t, tag, ts1)

	tag = tags.Intersection[int]()
	assert.Equal(t, 0, tag.Count())
}

func TestTags_Bytes(t *testing.T) {
	t.Parallel()

	tag := tags.NewTags(2, 18)
	assert.Equal(t, 3, len(tag.Bytes()))
}

func TestTags_Load(t *testing.T) {
	t.Parallel()

	tag1 := tags.NewTags(2, 18)
	tag2 := tags.NewTags[int]()

	tag2.Load(tag1.Bytes())
	assert.Equal(t, tag1.String(), tag2.String())
}
