package tags

import (
	"math/bits"
	"sort"
	"strconv"
	"strings"

	"gitee.com/xuender/oils/base"
	"golang.org/x/exp/constraints"
)

// Tags 标签.
type Tags[T constraints.Integer] []byte

const ten = 10

// NewTags 新建标签.
func NewTags[T constraints.Integer](elems ...T) Tags[T] {
	t := Tags[T]{}

	return t.Add(elems...)
}

// Add 增加标签.
func (t *Tags[T]) Add(elems ...T) Tags[T] {
	for _, tag := range elems {
		num, bytes := Int2Byte(tag)

		if int(num) >= len(*t) {
			*t = append(*t, make([]byte, int(num+1)-len(*t))...)
		}

		(*t)[num] |= bytes
	}

	return *t
}

// AddTags 增加标签.
func (t *Tags[T]) AddTags(elem Tags[T]) Tags[T] {
	if len(elem) > len(*t) {
		*t = append(*t, make([]byte, len(elem)-len(*t))...)
	}

	for i, tag := range elem {
		(*t)[i] |= tag
	}

	return *t
}

// All 全部包含.
func (t Tags[T]) All(elems ...T) bool {
	for _, tag := range elems {
		if !t.Has(tag) {
			return false
		}
	}

	return true
}

// Any 包含任意.
func (t Tags[T]) Any(elems ...T) bool {
	for _, tag := range elems {
		if t.Has(tag) {
			return true
		}
	}

	return false
}

// Bytes 字节码.
func (t Tags[T]) Bytes() []byte {
	return []byte(t)
}

// Count 标签数量.
func (t Tags[T]) Count() int {
	count := 0
	for _, b := range t {
		count += bits.OnesCount8(b)
	}

	return count
}

// Del 删除标签.
func (t Tags[T]) Del(elems ...T) Tags[T] {
	for _, tag := range elems {
		n, b := Int2Byte(tag)

		if int(n) < len(t) {
			t[n] &= ^b
		}
	}

	return t
}

// DelTags 删除标签.
func (t Tags[T]) DelTags(elem Tags[T]) Tags[T] {
	l := len(t)
	if l > len(elem) {
		l = len(elem)
	}

	for i := 0; i < l; i++ {
		t[i] &= ^elem[i]
	}

	return t
}

// Has 包含标签.
func (t Tags[T]) Has(tag T) bool {
	n, b := Int2Byte(tag)

	return int(n) < len(t) && (t[n]&b) > 0
}

// Join 集合连接.
func (t Tags[T]) Join(sep string) string {
	elems := t.Slice()

	switch len(elems) {
	case 0:
		return ""
	case 1:
		return strconv.FormatInt(int64(elems[0]), ten)
	}

	var builder strings.Builder

	builder.WriteString(strconv.FormatInt(int64(elems[0]), ten))

	for _, i := range elems[1:] {
		builder.WriteString(sep)
		builder.WriteString(strconv.FormatInt(int64(i), ten))
	}

	return builder.String()
}

// Load 加载.
func (t *Tags[T]) Load(bs []byte) {
	*t = bs
}

// Slice 转换切片.
func (t Tags[T]) Slice() []T {
	ret := base.NewSlice[T]()

	for n, b := range t {
		ret.Add(Byte2Ints(T(n), b)...)
	}

	sort.Sort(ret)

	return ret
}

// String 字符串.
func (t Tags[T]) String() string {
	return t.Join(", ")
}

// Intersection 交集.
// nolint
func Intersection[T constraints.Integer](elems ...Tags[T]) Tags[T] {
	switch len(elems) {
	case 0:
		return Tags[T]{}
	case 1:
		return elems[0]
	}

	ret := Tags[T]{}
	ret.AddTags(elems[0])

	for _, elem := range elems[1:] {
		if len(ret) > len(elem) {
			ret = ret[:len(elem)]
		}

		for i := range ret {
			ret[i] &= elem[i]
		}
	}

	return ret
}
