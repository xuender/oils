package oss_test

import (
	"os"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/oss"
)

func TestReadLine(t *testing.T) {
	t.Parallel()

	count := 0

	assert.Nil(t, oss.ReadLine("../LICENSE", func(line string) error {
		count++

		return nil
	}))
	assert.Equal(t, 21, count)
}

func TestReadLine_Error(t *testing.T) {
	t.Parallel()

	assert.NotNil(t, oss.ReadLine("../LICENSE", func(line string) error {
		return os.ErrNotExist
	}))
}
