package oss_test

import (
	"io/fs"
	"path/filepath"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/oss"
)

func TestWalkDir(t *testing.T) {
	t.Parallel()

	_ = oss.WalkDir("/tmp", func(path string, d fs.DirEntry, err error) error {
		assert.LessOrEqual(t, len(filepath.SplitList(path)), 3)

		return nil
	}, 2)

	count := 0

	_ = oss.WalkDir("/tmp", func(path string, d fs.DirEntry, err error) error {
		count++

		return nil
	}, 0)

	assert.Equal(t, 1, count)
}
