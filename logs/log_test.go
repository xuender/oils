package logs_test

import (
	"os"
	"path/filepath"
	"runtime"
	"testing"

	. "gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/logs"
)

func TestLogName(t *testing.T) {
	t.Parallel()

	switch runtime.GOOS {
	case "window":
		Equal(t, filepath.Join(filepath.Base(os.Args[0]), "logs.log"), logs.LogName())
	default:
		Equal(t, "/var/tmp/logs.log", logs.LogName())
	}
}
