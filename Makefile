lint:
	golangci-lint run
mock:
	# mockgen -source ./level/db.go -destination ./level/db_mock_test.go -package=level_test -aux_files=gitee.com/xuender/oils/level=level/geter.go,gitee.com/xuender/oils/level=level/puter.go

	mockgen -source ./logs/logger.go -destination ./logs/logger_mock_test.go -package=logs_test
	mockgen -source ./level/db.go -destination ./level/db_mock_test.go -package=level_test
	mockgen -destination=./level/iter_mock_test.go -package=level_test github.com/syndtr/goleveldb/leveldb/iterator Iterator
proto:
	protoc --go_out=plugins=grpc:. pb/*.proto
