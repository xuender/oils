module gitee.com/xuender/oils

go 1.18

require (
	github.com/dchest/siphash v1.2.3
	github.com/denisbrodbeck/machineid v1.0.1
	github.com/golang/mock v1.6.0
	github.com/google/uuid v1.3.0
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/retailnext/hllpp v1.0.0
	github.com/syndtr/goleveldb v1.0.0
	github.com/zserge/lorca v0.1.10
	go.uber.org/zap v1.21.0
	golang.org/x/exp v0.0.0-20220426173459-3bcf042a4bf5
	google.golang.org/protobuf v1.28.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gorm.io/gorm v1.23.5
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	golang.org/x/net v0.0.0-20220425223048-2871e0cb64e4 // indirect
	golang.org/x/sys v0.0.0-20220422013727-9388b58f7150 // indirect
)
