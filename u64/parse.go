package u64

import "strconv"

const (
	i10 = 10
	i64 = 64
)

func Parse(str string) uint64 {
	ret, _ := strconv.ParseInt(str, i10, i64)

	return uint64(ret)
}
