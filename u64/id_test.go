package u64_test

import (
	"fmt"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/base"
	"gitee.com/xuender/oils/u64"
)

func ExampleNewID() {
	fmt.Println(u64.NewID(3))
	id := u64.ID()
	fmt.Println(id)
	fmt.Println(u64.DecodeID(id))
}

func ExampleID() {
	fmt.Println(u64.ID())
	fmt.Println(u64.ID())
}

func TestNewID(t *testing.T) {
	t.Parallel()

	set := base.NewSet[uint64]()

	for i := 0; i < 100_000; i++ {
		set.Add(u64.NewID(3))
	}

	assert.Equal(t, 100_000, len(set))
}

func TestID(t *testing.T) {
	t.Parallel()

	set := base.NewSet[uint64]()

	for i := 0; i < 100_000; i++ {
		set.Add(u64.ID())
	}

	assert.Equal(t, 100_000, len(set))
}

func BenchmarkID(b *testing.B) {
	for i := 0; i < b.N; i++ {
		u64.ID()
	}
}

func BenchmarkNewID(b *testing.B) {
	for i := 0; i < b.N; i++ {
		u64.NewID(3)
	}
}

// func BenchmarkID2(b *testing.B) {
// 	for i := 0; i < b.N; i++ {
// 		u64.ID2()
// 	}
// }

func TestDecodeID(t *testing.T) {
	t.Parallel()

	id := u64.ID()
	ts, serial, machine := u64.DecodeID(id)

	assert.Greater(t, ts, 100, "ts")
	assert.GreaterOrEqual(t, serial, 0, "serial")
	assert.GreaterOrEqual(t, machine, 0, "machine")
}
