package u64_test

import (
	"fmt"
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/u64"
)

func ExampleRand() {
	fmt.Println(u64.Rand())
}

func TestRand(t *testing.T) {
	t.Parallel()

	for i := 0; i < 1000; i++ {
		assert.NotEqual(t, u64.Rand(), u64.Rand())
		assert.Greater(t, uint64(1<<54), u64.Rand())
	}
}
