package u64

import "encoding/binary"

// Bytes uint64 to []byte.
func Bytes(i uint64) []byte {
	e := 8
	bytes := make([]byte, e)

	binary.BigEndian.PutUint64(bytes, i)

	return bytes
}

// Uint64 []byte to Uint64.
func Uint64(bytes []byte) uint64 {
	return binary.BigEndian.Uint64(bytes)
}

// Bytes 转换字节码.
func (u U64) Bytes() []byte {
	return Bytes(uint64(u))
}
