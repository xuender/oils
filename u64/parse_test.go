package u64_test

import (
	"testing"

	. "gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/u64"
)

func TestParse(t *testing.T) {
	t.Parallel()

	Equal(t, 1, u64.Parse("1"))
}
