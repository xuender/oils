package u64_test

import (
	"testing"

	"gitee.com/xuender/oils/assert"
	"gitee.com/xuender/oils/u64"
)

func TestU64_Bytes(t *testing.T) {
	t.Parallel()

	u := u64.U64(33)
	bs := u.Bytes()

	assert.Equals(t, []byte{0, 0, 0, 0, 0, 0, 0, 33}, bs)
}

func TestUint64(t *testing.T) {
	t.Parallel()

	i := u64.Uint64([]byte{0, 0, 0, 0, 0, 0, 0, 33})

	assert.Equal(t, uint64(33), i)
}
